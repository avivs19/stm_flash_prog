/*
 * Copyright (c) 2020, Arm Limited and affiliates.
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <cstdint>
#include <cstring>
#include "SFDP.h"

/* Extracts Parameter ID MSB from the second DWORD of a parameter header */
inline uint8_t sfdp_get_param_id_msb(uint32_t dword2)
{
    return (dword2 & 0xFF000000) >> 24;
}

/* Extracts Parameter Table Pointer from the second DWORD of a parameter header */
inline uint32_t sfdp_get_param_tbl_ptr(uint32_t dword2)
{
    return dword2 & 0x00FFFFFF;
}

// Erase Types Params
const int SFDP_BASIC_PARAM_TABLE_ERASE_TYPE_1_BYTE = 29; ///< Erase Type 1 Instruction
const int SFDP_BASIC_PARAM_TABLE_ERASE_TYPE_1_SIZE_BYTE = 28; ///< Erase Type 1 Size
const int SFDP_BASIC_PARAM_TABLE_4K_ERASE_TYPE_BYTE = 1; ///< 4 Kilobyte Erase Instruction

const int SFDP_ERASE_BITMASK_TYPE_4K_ERASE_UNSUPPORTED = 0xFF;

/** Parse SFDP Header
 * @param sfdp_hdr_ptr Pointer to memory holding an SFDP header
 * @return Number of Parameter Headers on success, -1 on failure
 */
int sfdp_parse_sfdp_header(sfdp_hdr *sfdp_hdr_ptr)
{
    if (!(std::memcmp(sfdp_hdr_ptr, "SFDP", 4) == 0 && sfdp_hdr_ptr->R_MAJOR == 1)) 
    {
        /*tr_error("Verify SFDP signature and version Failed");*/
        return -1;
    }

    /*tr_debug("Verified SFDP Signature and version successfully");*/

    int hdr_cnt = sfdp_hdr_ptr->NPH + 1;
    /*tr_debug("Number of parameter headers: %d", hdr_cnt);*/

    return hdr_cnt;
}

/**********************************************************************************/


/** Parse Parameter Header
 * @param phdr_ptr Pointer to memory holding a single SFDP Parameter header
 * @param hdr_info Reference to a Parameter Table structure where info about the table is written
 * @return 0 on success, -1 on failure
 */
int sfdp_parse_single_param_header(sfdp_prm_hdr *phdr_ptr, sfdp_hdr_info &hdr_info)
{
    if (phdr_ptr->P_MAJOR != 1) 
    {
        /*tr_error("Parameter header: Major Version must be 1!");*/
        return -1;
    }

    if ((phdr_ptr->PID_LSB == 0) && (sfdp_get_param_id_msb(phdr_ptr->DWORD2) == 0xFF)) 
    {
        /*tr_debug("Parameter header: Basic Parameter Header");*/
        hdr_info.bptbl.addr = sfdp_get_param_tbl_ptr(phdr_ptr->DWORD2);
        hdr_info.bptbl.size = std::min((phdr_ptr->P_LEN * 4), SFDP_BASIC_PARAMS_TBL_SIZE);

    } 
    else if ((phdr_ptr->PID_LSB == 0x81) && (sfdp_get_param_id_msb(phdr_ptr->DWORD2) == 0xFF)) 
    {
        /*tr_debug("Parameter header: Sector Map Parameter Header");*/
        hdr_info.smptbl.addr = sfdp_get_param_tbl_ptr(phdr_ptr->DWORD2);
        hdr_info.smptbl.size = phdr_ptr->P_LEN * 4;

    } 
    else 
    {
        /*tr_debug("Parameter header: header vendor specific or unknown. Parameter ID LSB: 0x%" PRIX8 "; MSB: 0x%" PRIX8 "",
                 phdr_ptr->PID_LSB,
                 sfdp_get_param_id_msb(phdr_ptr->DWORD2));*/
    }

    return 0;
}

/**********************************************************************************/

uint32_t sfdp_detect_page_size(uint8_t *basic_param_table_ptr, uint32_t basic_param_table_size)
{
    const int SFDP_BASIC_PARAM_TABLE_PAGE_SIZE = 40;
    const int SFDP_DEFAULT_PAGE_SIZE = 256;

    unsigned int page_size = SFDP_DEFAULT_PAGE_SIZE;

    if (basic_param_table_size > SFDP_BASIC_PARAM_TABLE_PAGE_SIZE) 
    {
        // Page Size is specified by 4 Bits (N), calculated by 2^N
        int page_to_power_size = ((int)basic_param_table_ptr[SFDP_BASIC_PARAM_TABLE_PAGE_SIZE]) >> 4;
        page_size = 1 << page_to_power_size;
        /*tr_debug("Detected Page Size: %d", page_size);*/
    } 
    else 
    {
        /*tr_debug("Using Default Page Size: %d", page_size);*/
    }
    return page_size;
}

/**********************************************************************************/

int sfdp_detect_erase_types_inst_and_size(uint8_t *bptbl_ptr, sfdp_hdr_info &sfdp_info)
{
    uint8_t bitfield = 0x01;

    // Erase 4K Inst is taken either from param table legacy 4K erase or superseded by erase Instruction for type of size 4K
    if (sfdp_info.bptbl.size > SFDP_BASIC_PARAM_TABLE_ERASE_TYPE_1_SIZE_BYTE) 
    {
        // Loop Erase Types 1-4
        for (int i_ind = 0; i_ind < 4; i_ind++) 
        {
            sfdp_info.smptbl.erase_type_inst_arr[i_ind] = -1; // Default for unsupported type
            sfdp_info.smptbl.erase_type_size_arr[i_ind] = 1
                                                          << bptbl_ptr[SFDP_BASIC_PARAM_TABLE_ERASE_TYPE_1_SIZE_BYTE + 2 * i_ind]; // Size is 2^N where N is the table value
            /*tr_debug("Erase Type(A) %d - Inst: 0x%xh, Size: %d", (i_ind + 1), sfdp_info.smptbl.erase_type_inst_arr[i_ind],
                     sfdp_info.smptbl.erase_type_size_arr[i_ind]);*/
            if (sfdp_info.smptbl.erase_type_size_arr[i_ind] > 1) 
            {
                // if size==1 type is not supported
                sfdp_info.smptbl.erase_type_inst_arr[i_ind] = bptbl_ptr[SFDP_BASIC_PARAM_TABLE_ERASE_TYPE_1_BYTE
                                                                        + 2 * i_ind];

                if ((sfdp_info.smptbl.erase_type_size_arr[i_ind] < sfdp_info.smptbl.regions_min_common_erase_size)
                        || (sfdp_info.smptbl.regions_min_common_erase_size == 0)) 
                {
                    //Set default minimal common erase for signal region
                    sfdp_info.smptbl.regions_min_common_erase_size = sfdp_info.smptbl.erase_type_size_arr[i_ind];
                }
                sfdp_info.smptbl.region_erase_types_bitfld[0] |= bitfield; // If there's no region map, set region "0" types bitfield as default
            }

            /*tr_debug("Erase Type %d - Inst: 0x%xh, Size: %d", (i_ind + 1), sfdp_info.smptbl.erase_type_inst_arr[i_ind],
                     sfdp_info.smptbl.erase_type_size_arr[i_ind]);*/
            bitfield = bitfield << 1;
        }
    } else 
    {
        /*tr_debug("Erase types are not available - falling back to legacy 4k erase instruction");*/

        sfdp_info.bptbl.legacy_erase_instruction = bptbl_ptr[SFDP_BASIC_PARAM_TABLE_4K_ERASE_TYPE_BYTE];
        if (sfdp_info.bptbl.legacy_erase_instruction == SFDP_ERASE_BITMASK_TYPE_4K_ERASE_UNSUPPORTED) 
        {
            /*tr_error("Legacy 4k erase instruction not supported");*/
            return -1;
        }
    }

    return 0;
}

/**********************************************************************************/

int sfdp_find_addr_region(uint32_t offset, const sfdp_hdr_info &sfdp_info)
{
    if ((offset > sfdp_info.bptbl.device_size_bytes) || (sfdp_info.smptbl.region_cnt == 0)) 
    {
        return -1;
    }

    if (sfdp_info.smptbl.region_cnt == 1) 
    {
        return 0;
    }

    for (int i_ind = sfdp_info.smptbl.region_cnt - 2; i_ind >= 0; i_ind--) 
    {
        if (offset > sfdp_info.smptbl.region_high_boundary[i_ind]) 
        {
            return (i_ind + 1);
        }
    }
    return -1;

}

/**********************************************************************************/

int sfdp_iterate_next_largest_erase_type(uint8_t &bitfield,
                                         int size,
                                         int offset,
                                         int region,
                                         const sfdp_smptbl_info &smptbl)
{
    uint8_t type_mask = SFDP_ERASE_BITMASK_TYPE4;
    int i_ind = 0;
    int largest_erase_type = 0;
    for (i_ind = 3; i_ind >= 0; i_ind--) 
    {
        if (bitfield & type_mask) 
        {
            largest_erase_type = i_ind;
            if ((size > (int)(smptbl.erase_type_size_arr[largest_erase_type])) &&
                    ((smptbl.region_high_boundary[region] - offset)
                     > (int)(smptbl.erase_type_size_arr[largest_erase_type]))) 
            {
                break;
            } 
            else 
            {
                bitfield &= ~type_mask;
            }
        }
        type_mask = type_mask >> 1;
    }

    if (i_ind == 4) 
    {
    /*    tr_error("No erase type was found for current region addr");*/
    }
    return largest_erase_type;
}
